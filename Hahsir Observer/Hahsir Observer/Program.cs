﻿using System;
using System.Collections.Generic;
namespace ObserverDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Laptop BlackDell = new Laptop("Black Dell Laptop", 70000, "Out Of Stock");
            Observer user1 = new Observer("Mr.Ahmed", BlackDell);
            Observer user2 = new Observer("MR.Hamza", BlackDell);
            Observer user3 = new Observer("MR.Rohail", BlackDell);

            Console.WriteLine("Black Dell Laptop current state : " + BlackDell.getAvailability());
            Console.WriteLine();

            BlackDell.setAvailability("Available");
            Console.Read();
        }
    }
}
public interface ILaptop
{
    void RegisterObserver(IObserver observer);
    void RemoveObserver(IObserver observer);
    void NotifyObservers();
}
public class Laptop : ILaptop
{
    private List<IObserver> observers = new List<IObserver>();
    private string ProductName { get; set; }
    private int ProductPrice { get; set; }
    private string Availability { get; set; }
    public Laptop(string productName, int productPrice, string availability)
    {
        ProductName = productName;
        ProductPrice = productPrice;
        Availability = availability;
    }

    public string getAvailability()
    {
        return Availability;
    }
    public void setAvailability(string availability)
    {
        this.Availability = availability;
        Console.WriteLine("Availability changed from Out of Stock to Available.");
        NotifyObservers();
    }
    public void RegisterObserver(IObserver observer)
    {
        Console.WriteLine("Observer Added : " + ((Observer)observer).UserName);
        observers.Add(observer);
    }
    public void AddObservers(IObserver observer)
    {
        observers.Add(observer);
    }
    public void RemoveObserver(IObserver observer)
    {
        observers.Remove(observer);
    }
    public void NotifyObservers()
    {
        Console.WriteLine("Product Name :"
                        + ProductName + ", product Price : "
                        + ProductPrice + " is Now available. So notifying all Registered users ");
        Console.WriteLine();
        foreach (IObserver observer in observers)
        {
            observer.update(Availability);
        }
    }
}
public interface IObserver
{
    void update(string availability);
}
public class Observer : IObserver
{
    public string UserName { get; set; }
    public Observer(string userName, ILaptop laptop)
    {
        UserName = userName;
        laptop.RegisterObserver(this);
    }

    public void update(string availabiliy)
    {
        Console.WriteLine("Hello " + UserName + ", Product is now " + availabiliy + " on Laptop Zone");
    }
}

