﻿using System;

namespace Latop_Zone_DECORATOR
{
    class Program
    {
        static void Main(string[] args)
        {
            Laptop myLaptop = new HP();
            Console.WriteLine(myLaptop.GetPrice());
            Console.WriteLine(myLaptop.GetDescription());
            myLaptop = new SSDCARD(myLaptop);
            Console.WriteLine(myLaptop.GetPrice());
            Console.WriteLine(myLaptop.GetDescription());
            myLaptop = new SDCARD(myLaptop);
            Console.WriteLine(myLaptop.GetPrice());
            Console.WriteLine(myLaptop.GetDescription());
        }
    }
}

public abstract class Laptop
{
    public abstract string GetDescription();

    public abstract double GetPrice();

    public string Description { get; set; }
}

public class Dell : Laptop
{
    public Dell()
    {
        Description = "Dell Laptop";
    }

    public override string GetDescription()
    {
        return Description;
    }

    public override double GetPrice()
    {
        return 104.10;
    }
}

public class HP : Laptop
{
    public HP()
    {
        Description = "HP Laptop";
    }

    public override string GetDescription()
    {
        return Description;
    }

    public override double GetPrice()
    {
        return 215.55;
    }
}

public class SSDCARD : LaptopDecorator
{
    public SSDCARD(Laptop laptop) : base(laptop)
    {
        Description = "SSDCARD";
    }

    public override string GetDescription()
    {
        return Laptop.GetDescription() + ", " + Description;
    }

    public override double GetPrice()
    {
        return Laptop.GetPrice() + 4.55;
    }
}

public class SDCARD : LaptopDecorator
{
    public SDCARD(Laptop laptop)
        : base(laptop)
    {
        Description = "SDCARD";
    }

    public override string GetDescription()
    {
        return Laptop.GetDescription() + ", " + Description;
    }

    public override double GetPrice()
    {
        return Laptop.GetPrice() + 1.23;
    }
}

public class LaptopDecorator : Laptop
{
    protected Laptop Laptop;

    public LaptopDecorator(Laptop laptop)
    {
        Laptop = laptop;
    }

    public override string GetDescription()
    {
        return Laptop.GetDescription();
    }

    public override double GetPrice()
    {
        return Laptop.GetPrice();
    }
}