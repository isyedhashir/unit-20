﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Builder
{
    // Laptop class made
    public class Laptop
    {
        // Public properties to access laptop parts
        public string Ram { get; set; }
        public string Processor { get; set; }
        public string OS { get; set; }
        public string ScreenSize { get; set; }
        //Intializing and using parameters
        public Laptop(string _Ram, string _Processor, string _OS, string _ScreenSize)
        {
            this.Ram = _Ram;
            this.Processor = _Processor;
            this.OS = _OS;
            this.ScreenSize = _ScreenSize;
        }
        public Laptop()
        {
        }
    }
    //This is Builder class
    public class LaptopBuilder
    {
        private Laptop laptop;
        public Laptop SetOS(string os)
        {
            laptop.OS = os;
            return laptop;
        }
        public Laptop SetRAM(string ram)
        {
            laptop.Ram = ram;
            return laptop;
        }
        public Laptop SetProcessor(string processor)
        {
            laptop.Processor = processor;
            return laptop;
        }
        public Laptop SetScreenSize(string screenSize)
        {
            laptop.ScreenSize = screenSize;
            return laptop;
        }
        // Methiod to display phone details in our own representation
        public void GetLaptop()
        {
            Console.WriteLine("laptop details are : OS=" + laptop.OS + ", RAM=" + laptop.Ram);
        }
        public LaptopBuilder()
        {
            laptop = new Laptop();
        }
    }
    class Program
    {
        public static void Main()
        {
            Laptop ph1 = new Laptop("4GB", "5.5Ghz", "Windows", "12.5");
            LaptopBuilder ph2 = new LaptopBuilder();
            ph2.SetOS("Widows");
            ph2.SetRAM("4GB");
            ph2.GetLaptop();
            Console.ReadKey();
        }
    }
}